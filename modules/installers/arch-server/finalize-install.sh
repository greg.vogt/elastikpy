#!/bin/bash

username="as"

{
figlet Final-Installation

# Enable important services
systemctl enable sshd
#systemctl enable acpid
#systemctl enable ntpd
systemctl enable cronie
systemctl enable auditd
systemctl enable logwatch.timer
#systemctl enable avahi-daemon
systemctl enable fstrim.timer
systemctl enable netctl
systemctl enable systemd-resolved
systemctl enable docker.service

systemctl enable fail2ban.service
systemctl enable iptables.service
systemctl enable ip6tables.service

# Enable network
netctl enable static_linode

# Configure Keyboardlayout
localectl set-x11-keymap us pc105

# Compile and send over user-install.sh
cat /elastik/modules/arch-server.mod/user-install.sh > /home/$username/user-install.sh

chown $username:users /home/$username/user-install.sh
chmod +x /home/$username/user-install.sh

# Remove installation files
rm -R /elastik/

# Lock root user
passwd -l root

# Clean cache and database files ~600MB
pacman -Scc

# Finish Installation
echo "Please remember to run User-Installation.sh in /home/$username"
read -p "Finished! Press enter to reboot..."

# Pipe all output into log file
} |& tee -a /home/$username/Elastik-Finalize-Installation.log

chown $username:users/home/$username/Elastik-Finalize-Installation.log
chmod 600 /home/$username/Elastik-Finalize-Installation.log

reboot
