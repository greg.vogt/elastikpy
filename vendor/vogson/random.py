#!/usr/bin/env python3

import string, random

# https://stackoverflow.com/a/2257449
def ran_gen(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def crypt_gen(size=6):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(size))