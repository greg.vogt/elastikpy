#!/bin/bash

# Usage: input_box "script name" "name" "type(ie: passwordbox or inputbox)" "message" "default text" "confirm(boolean)" "confirm message"
function input_box {
  unset input
  # Duplicate descriptor 1 on descriptor 3
  exec 3>&1
  input=$(dialog --title "$SCRIPT_NAME: $1 - $2" \
    --clear \
    --$3 "$4" 0 0 "$5" 2>&1 1>&3)

    response=$?

    case $response in
      $DIALOG_OK)
      # Checks if our user input is set and if not restart the process
      if ! test -z "$input" ; then
        # This bit of jumbled up logic is to decide whether our script has called
        # for a confirm box to be used. If not we just return our variable.
        if $6 = "true"; then
          confirm=$(dialog --title "$SCRIPT_NAME: Interactive - $1 - Confirm" \
            --clear \
            --insecure \
            --$3 "$7" 0 0 2>&1 1>&3)
            echo "Confirm: $confirm"
            echo "Input: $input"
          # Here we check if the users input matches, if not we restart the function.
          if [[ "$confirm" = "$input" ]]; then
            # Return user input
            #
            # Since bash doesnt play nice with returning strings
            # we'll go ahead and send a temp output variable.
            # Its recommended that you place this in a more
            # suitable home immediately because it will be
            # overwritten next call to this function.
            tmp_output="$input"
          else
            dialog --title "$SCRIPT_NAME: Interactive - $1" \
              --clear \
              --insecure \
              --msgbox "Confirm mismatch! Please try again." 0 0

              input_box "$1" "$2" "$3" "$4" "$5" "$6" "$7"
          fi
        else
          # Return user input
          #
          # Since bash doesnt play nice with returning strings
          # we'll go ahead and send a temp output variable.
          # Its recommended that you place this in a more
          # suitable home immediately because it will be
          # overwritten next call to this function.
          tmp_output="$input"
        fi
      else
        # If our check returns false we'll restart the function
        input_box "$1" "$2" "$3" "$4" "$5" "$6" "$7"
      fi
      ;;
      $DIALOG_CANCEL)
        dialog --title "$SCRIPT_NAME" \
          --clear  \
          --yesno "Are you sure you'd like to cancel? This will exit the script." 0 0

        case $? in
          $DIALOG_CANCEL)
            clear
            input_box "$1" "$2" "$3" "$4" "$5" "$6" "$7"
            ;;
          $DIALOG_OK)
            clear
            echo "Program aborted." >&2
            exit 1
            ;;
        esac
      ;;
      $DIALOG_ESC)
        dialog --title "$SCRIPT_NAME" \
          --clear  \
          --yesno "Are you sure you'd like to cancel? This will exit the script." 0 0

        case $? in
          $DIALOG_CANCEL)
            clear
            input_box "$1" "$2" "$3" "$4" "$5" "$6" "$7"
            ;;
          $DIALOG_OK)
            clear
            echo "Program aborted." >&2
            exit 1
            ;;
        esac
      ;;
    esac
    #Close file descriptor 3
    exec 3>&-
}

# Usage: yesno "script name" "name" "message" "No message" "Yes message"
function yesno {
  dialog --title "$SCRIPT_NAME: $1 - $2" \
    --clear \
    --yesno "$3" 0 0

  case $? in
    $DIALOG_CANCEL)
      clear
      echo "$4"
      ;;
    $DIALOG_OK)
      clear
      echo "$5"
      ;;
  esac
}

# Usage: "module name" "step" "option array"
#
# Known Limitations:
#
# Spaces in array values break dialog since I'm unable to
# encapsulate string in quotations before passing the value
#
# Current fix: underscores ie: "Open_Dialog" 
#
# More than likely just a limiation in my knowledge with
# bash, please help... 
function select_dialog {
  for i in ${!3[@]}; do
    # Add 1 to i since 0 is a termination trigger
    options+="$((i+1)) ${3[$i]} "
  done

  select=$(dialog --title "$SCRIPT_NAME: $1 - $2" \
    --stdout \
    --clear \
    --cancel-label "Exit" \
    --menu "Please select: " 0 0 4 \
    ${options} )

  exit_status=$?

  case $exit_status in
    $DIALOG_CANCEL)
      clear
      echo "Program terminated."
      exit
      ;;
    $DIALOG_ESC)
      clear
      echo "Program aborted." >&2
      exit 1
      ;;
  esac

  tmp_output="$select"
}

function dir_checksum {
# Skips index.vs
if [[ $1 != "index.vs" ]]; then
  # Runs sha256 checksum compare
  if [[ ${index[$1]} == $2 ]]; then
      # Checksum matches - continue
      echo "Integrity check pass: $1"
    else
      # Checksum mismatch - halt
      # Send msg_box command
      input_box "Integrity Mismatch" "msgbox" "File integrity mismatch: $1" "false" ""
    fi
  fi
}

#Usage: create_checksums (directory)
function create_checksums {
  for file in $(dir -A $1); do
    if [[ $file != "configs" ]]; then
      checksum=$(sha256sum $1/${file##*/})
      IFS=' ' read -r -a entry <<< "$checksum"
      #dir_checksum $file $entry
      echo "['$file']=$entry \\"
    fi
  done
}


#Usage: module_run (path/to/index.vs)
function module_run {
  . $1/index.sh

  #python3.6 $PWD/sql_connect.py create_db $mysql_ip $mysql_user $mysql_pass $mysql_db

  #python3.6 $PWD/sql_connect.py create_table $mysql_ip $mysql_user $mysql_pass $mysql_db $mysql_table
  touch $PWD/logs
  i=1
  until [ $i -eq $((${#index_sorted[@]}+1)) ]; do

  # Neat while loop to tell us what script is being run at what time;
  # incompatible with expect since we intercept and break IFS.
  
  #  sql_create_table ${index_sorted[$i]}
  #  while IFS= read -r line ; do
  #      echo "[${index_sorted[$i]}]:[$(date +"%y-%m-%d %T")] $line"
  #      echo "[${index_sorted[$i]}]:[$(date +"%y-%m-%d %T")] $line" >> $PWD/logs
  #      #python3.6 $PWD/sql_connect.py log $mysql_ip $mysql_user $mysql_pass $mysql_db $mysql_table "$line" "${index_sorted[$i]}"
  #  done < <(. $1/${index_sorted[$i]} 2>&1)

    . $1/${index_sorted[$i]}
    ((i++))
  done
}


#create_checksums /root/machine/Vogsuite/harden/
