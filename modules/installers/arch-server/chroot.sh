#!/bin/bash

# Install base packages
figlet Installing Base Packages
pacstrap /mnt base base-devel linux linux-headers

# Copy the setup folder to the new system
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Use /elastik/ as operating dir when chrooted
cp -R $ELASTIKBASE /mnt/elastikpy

# Generate fstab
genfstab -U -p /mnt >> /mnt/etc/fstab

# Copy pacman mirrorlist over
cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist

# Compile and send over chroot-runner.sh
# The blank echo is to ensure every concatenation is on a new line
touch $ELASTIKBASE/volatile/chroot-runner.sh
echo "#!/bin/bash" > $ELASTIKBASE/volatile/chroot-runner.sh
echo " " >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "{" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo " " >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "disk='${disk}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "hostname='${hostname}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "username='${username}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "userpass='${userpass}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "ssh_key='${ssh_key}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "root_password='${root_password}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "bootloader='${bootloader}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "email='${email}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "ssh_ipv4='${ssh_ipv4}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "ssh_ipv6='${ssh_ipv6}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "ssh_port='${ssh_port}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "email_host='${email_host}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "email_user='${email_user}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "email_password='${email_password}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "ipv4='${ssh_ipv4}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "ipv4_gw='${ipv4_gw}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "ipv6='${ssh_ipv6}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "ipv6_gw='${ipv6_gw}'" >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "MODULEDIR='${SCRIPTPATH}'" >> $ELASTIKBASE/volatile/chroot-runner.sh

echo " " >> $ELASTIKBASE/volatile/chroot-runner.sh
cat $SCRIPTPATH/base-system.sh >> $ELASTIKBASE/volatile/chroot-runner.sh
echo " " >> $ELASTIKBASE/volatile/chroot-runner.sh
cat $SCRIPTPATH/post-setup.sh >> $ELASTIKBASE/volatile/chroot-runner.sh
echo " " >> $ELASTIKBASE/volatile/chroot-runner.sh
cat $SCRIPTPATH/harden.sh >> $ELASTIKBASE/volatile/chroot-runner.sh
echo " " >> $ELASTIKBASE/volatile/chroot-runner.sh
echo "} |& tee -a /elastik/Elastik-Chroot-Installation.log" >> $ELASTIKBASE/volatile/chroot-runner.sh

cp $ELASTIKBASE/volatile/chroot-runner.sh /mnt/elastikpy/chroot-runner.sh

# Compile and send over finalize-install.sh
sed -i "s/^username=.*/username=\"${username}\"/" $SCRIPTPATH/finalize-install.sh

cat $SCRIPTPATH/finalize-install.sh > /mnt/elastikpy/finalize-install.sh

# Set execute bit
chmod +x /mnt/elastikpy/chroot-runner.sh
chmod +x /mnt/elastikpy/finalize-install.sh

# Enter chroot
figlet Entering Chroot
arch-chroot /mnt /elastikpy/chroot-runner.sh
