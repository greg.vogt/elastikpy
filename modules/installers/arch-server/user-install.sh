figlet Installing AUR programs

# Install Trizen
wget https://aur.archlinux.org/cgit/aur.git/snapshot/trizen.tar.gz -O - | tar xz -C /tmp
chown -R $username:users /tmp/trizen
sudo -u $username bash -c 'cd /tmp/trizen && makepkg -si'

trizen -S aide

# Initialize Advanced Intrusion Detection Environment (AIDE)
sudo /usr/sbin/aide --init

sudo cp /var/lib/aide/aide.db.gz.new /var/lib/aide/aide.db.initial.gz

sudo tar -c --use-compress-program=pigz -f aide.conf-hash.tar.gz /var/lib/aide/aide.db.gz.new /etc/aide.conf

# Run first check and email results
sudo /usr/sbin/aide --check | mail -a aide.conf-hash.tar.gz -s "AIDE Hash Table and Conf for $HOSTNAME" $email

sudo rm aide.conf-hash.tar.gz

# Get oh-my-zsh
curl -L http://install.ohmyz.sh | sh

# /bin/zsh is in our verified shells list
chsh -s /bin/zsh

# Change OhMyZsh theme
sed -i "s/ZSH_THEME=\"robbyrussell\"/ZSH_THEME=\"agnoster\"/g" ~/.zshrc
