#!/bin/bash

# This file makes sure the environment is primed before escalating
# to the python environment.

# TODO: Add compatibility for other linux OSes

# Check if script is being ran as root or sudo user
if [[ $EUID -ne 0 ]]; then
  dialog --title "$SCRIPT_NAME" \
    --clear  \
    --msgbox "Elastik must be run as root!" 0 0
  exit 1
fi

# Define some of our global variables
: ${SCRIPT_NAME="Elastik V1.1"}

# Legacy; docker and iso will be primed already
# if [ ! -f $PWD/.ELASTIKPRIMED ]; then
#     touch $PWD/.ELASTIKPRIMED

#     # Select mirrors
    # pacman --noconfirm -Sy pacman-contrib reflector && \
	  # cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup && \
	  # reflector --verbose --protocol https --sort rate --save /etc/pacman.d/mirrorlist

#     # Install prerequisites
#     pacman --noconfirm -Sy figlet dialog jq expect python-pip

#     # Install Python modules
#     pip install --target $PWD/vendor netifaces
# fi

# Source our functions, separated for neatness
# Any bash script called from Python will need these
# files sourced again
. $PWD/base/base.sh
. $PWD/base/json.sh

find $PWD -type f -iname "*.sh" -exec chmod +x {} \;
find $PWD -type f -iname "*.py" -exec chmod +x {} \;

mkdir -p ${PWD}/volatile ${PWD}/logs

sed -i "s#^volatile_path .*#volatile_path = \"${PWD}/volatile\"#g" $PWD/vendor/vogson/signal.py
sed -i "s#^volatile_path .*#volatile_path = \"${PWD}/volatile\"#g" $PWD/modules/tools/arch-iso/prime.sh

$PWD/main.py "$SCRIPT_NAME"
