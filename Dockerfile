FROM archlinux:latest AS base
MAINTAINER Greg Vogt <greg.vogt@vogson.io>

RUN pacman --noconfirm -Sy pacman-contrib reflector && \
	cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup && \
	reflector --verbose --protocol https --sort rate --save /etc/pacman.d/mirrorlist && \
    pacman --noconfirm -Sy figlet git dialog jq expect wget base-devel python-pip p7zip \
        xorriso libffi arch-install-scripts squashfs-tools curl nano && \
    pip install netifaces

WORKDIR "/elastikpy"

CMD [ "/bin/bash" ]
#CMD [ "./elastik.sh" ]