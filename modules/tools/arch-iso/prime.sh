#!/bin/bash

# Better idea to allow each module its own temp dir?
volatile_path="added by inital launch"

DATE=$(date +"%Y.%m.01")                                    # Every ArchISO date ends in 01
MIRROR="https://mirror.rackspace.com/archlinux/iso/$DATE"   # Any Arch mirror should work here
BASE_DIR=$PWD                                               # EX: /root/ElastikPy
BUILD_DIR=$BASE_DIR/build
ISO_DIR=$BUILD_DIR/archlinux-$DATE-x86_64
ISO_IN=archlinux-$DATE-x86_64.iso                                         # For simplicity sake we'll name them the same
ISO_UNPACKED=$BUILD_DIR/iso/archlinux-$DATE-x86_64/unpacked

AIROOTFS_DIR=$ISO_UNPACKED/arch/x86_64
SQUASHFS_DIR=$ISO_UNPACKED/squashfs-root

SKIPCHECKS=0
################################################################################
# Help                                                                         #
################################################################################
help()
{
   # Display Help
   echo "Builds or launches ElastikPy Docker container"
   echo
   echo "Syntax: launch.sh [-g|h|v|V] <options>"
   echo "options:"
   echo "c     Clean build directory"
   echo "s     Skip checks"
   echo "h     Print this Help."
   echo
}

while getopts ':hcs' option; do
  case "$option" in
    h)  help
        exit
        ;;
    c)  # Cleanup any previous builds and make the tree again
        rm -rf $BUILD_DIR && mkdir -p $BUILD_DIR $ISO_DIR $ISO_UNPACKED
        ;;
    s)  # Skip checks
        SKIPCHECKS=1
        ;;
    :)  printf "missing argument for -%s\n" "$OPTARG" >&2
        help >&2
        exit 1
        ;;
    \?)  printf "illegal option: -%s\n" "$OPTARG" >&2
        help >&2
        exit 1
        ;;
  esac
done


if [ ! -f $ISO_DIR/$ISO_IN ]; then
    curl $MIRROR/$ISO_IN -o $ISO_DIR/$ISO_IN
    curl $MIRROR/$ISO_IN.sig -o $ISO_DIR/$ISO_IN.sig
    curl $MIRROR/sha1sums.txt -o $ISO_DIR/sha1sums.txt
    curl $MIRROR/md5sums.txt -o $ISO_DIR/md5sums.txt

    # Truncate last line of checksums since we dont use bootstrap
    sed -i '$d' $ISO_DIR/sha1sums.txt
    sed -i '$d' $ISO_DIR/md5sums.txt

    # Correct directory paths in the checksum files
    sed -i "s#$ISO_IN#$ISO_DIR/$ISO_IN#g" $ISO_DIR/sha1sums.txt
    sed -i "s#$ISO_IN#$ISO_DIR/$ISO_IN#g" $ISO_DIR/md5sums.txt

fi

check(){
    echo "$1 : $2"
    if [ $1 = 'gpg' ]; then
        gpg --keyserver https://pgp.mit.edu/ --keyserver-options auto-key-retrieve --verify $2
    elif [ $1 = 'sha1' ]; then
        sha1sum -c $2 &> /dev/null
    elif [ $1 = 'md5' ]; then
        md5sum -c $2 &> /dev/null
    elif [ $1 = 'sha256' ]; then
        sha256sum -c $2 &> /dev/null
    elif [ $1 = 'sha512' ]; then
        sha512sum -c $2 &> /dev/null
    fi
    [ $? -eq 0 ] && echo "[$1]: $ISO_IN verified" || { echo "[$1]: $ISO_IN verification failed"; exit 1; }
}

if [ SKIPCHECKS = 0 ]; then
    check gpg $ISO_DIR/$ISO_IN.sig
    check sha1 $ISO_DIR/sha1sums.txt
    check md5 $ISO_DIR/md5sums.txt
fi

# Arch Wiki Proper; doesnt work in docker
# mkdir -p $PWD/build/iso_mnt
# mount -t iso9660 -o loop $PWD/$ISO_IN $PWD/build/iso_mnt
# cp -a $PWD/build/iso_mnt $PWD/build/iso_out
# umount $PWD/build/iso_mnt

# osirrox alternative
osirrox -indev $ISO_DIR/$ISO_IN -extract / $ISO_UNPACKED

# Give absolute directory so SHA doesnt complain
sed -i "s#airootfs.sfs#$AIROOTFS_DIR/airootfs.sfs#g" $AIROOTFS_DIR/airootfs.sha512

if [ SKIPCHECKS = 0 ]; then
    check gpg $AIROOTFS_DIR/airootfs.sfs.sig
    check sha512 $AIROOTFS_DIR/airootfs.sha512
fi

unsquashfs -d $SQUASHFS_DIR $AIROOTFS_DIR/airootfs.sfs

# Copy the kernel over
cp $ISO_UNPACKED/arch/boot/x86_64/vmlinuz $SQUASHFS_DIR/boot/vmlinuz-linux
cp $BASE_DIR/chroot.sh $SQUASHFS_DIR/root/chroot.sh

arch-chroot $SQUASHFS_DIR /root/chroot.sh