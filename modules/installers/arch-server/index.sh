#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
ELASTIKBASE=$1
. $ELASTIKBASE/base/json.sh

json 0 $ELASTIKBASE/volatile/$2

declare -a index=(
  [1]="disks.sh" \
  [2]="chroot.sh"
)

. $SCRIPTPATH/disks.sh

. $SCRIPTPATH/chroot.sh