#!/usr/bin/env python3

# Wrapper for the Dialog.py
import sys, os, json, subprocess
from os.path import dirname, join, abspath, isdir

dirpath = abspath(join(dirname(__file__)))

sys.path.append(dirpath)

from vogson.signal import signal_handler

def mixedform(dialog, items):
    elements = []

    for i in range(len(items)):
        i1 = i + 1 # Since i += 1 doesnt work in a function call
        elements.append((items[i][0], i1, 0, items[i][1], i1, 15, 64, 64, 0))

    code, list = dialog.mixedform("Please fill out the form below:", elements)

    exits(dialog, code)

    vars = {}

    for i in range(len(list)):
        vars[items[i][0]] = list[i]

    return vars

def menu(script, dialog, path, basedir, type = "module"):

    modules = os.listdir(path)

    module = []

    # Gets length of modules array and then using range iterates over them
    for i in range(len(modules)):
            if os.path.isdir(path + "/" + modules[i]):
                with open(path + "/" + modules[i] + "/info.json", "r") as file_object:
                    module.append(json.load(file_object))

    choice = []

    # If the folder does not have any modules, safely catch it
    if module: 
        # Creates a dialog.py friendly array from the info.json file
        for i in range(len(module)):
            item = (module[i]['name'], module[i]['desc'])
            choice.append(item)
    
        code, tag = dialog.menu("Please select a module from the list:",
                        choices=choice)
    
        if code == dialog.ESC or code == dialog.CANCEL:
            signal_handler()
        else:
            if type == "module":
                # Gets folder name so we can trigger modules python entry script
                for m in module:
                    if tag == m['name']:
                        subprocess.call([path + "/" + m['folder'] + "/main.py", script, basedir])
                        break
            elif type == "category":
                # Returns selected path to category
                for m in module:
                    if tag == m['name']:
                        return path + "/" + m['folder']
                        break
    else:
        dialog.msgbox("It seems there aren't any modules here yet! Exiting.")
        signal_handler()



def exits(dialog, code):
    if code == dialog.ESC or code == dialog.CANCEL:
        signal_handler()