# No bash starter needed since this will be included in chroot-runner.sh

figlet Installing Important Services

# Configure the network
figlet Configuring Network
pacman -S --noconfirm dialog dhclient
pacman -S --noconfirm netctl
pacman -S --noconfirm gnome-keyring libsecret seahorse

# Samba
pacman -S --noconfirm bind-tools krb5 ntp openresolv samba

# Install command line and ncurses programs
figlet Command line and ncurses
pacman -S --noconfirm sudo zsh nano vim cronie bash-completion
pacman -S --noconfirm ca-certificates tcp-wrappers
pacman -S --noconfirm tree htop glances git
pacman -S --noconfirm wget curl axel s-nail msmtp msmtp-mta fail2ban pigz
pacman -S --noconfirm rsync sshfs lshw nmap arch-audit logwatch
pacman -S --noconfirm openssh openvpn easy-rsa
pacman -S --noconfirm docker docker-compose

# Avahi provides local hostname resolution using a "hostname.local" naming scheme
# sed -i '/hosts:/s/mymachines/mymachines mdns_minimal [NOTFOUND=return]/' /etc/nsswitch.conf

# Configure sudo
sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

# Create user
figlet New User
useradd -m -g users -G wheel,docker,games,power,optical,storage,scanner,lp,audio,video -s /bin/bash $username

# Use expect to set user password
expect -c "
set timeout -1
spawn passwd $username

expect \"New password:\"
send -- \"$userpass\r\"
expect \"Retype new password:\"
send -- \"$userpass\r\"

expect eof
"

# Set SSH Key for user
mkdir /home/$username/.ssh
touch /home/$username/.ssh/authorized_keys
echo $ssh_key > /home/$username/.ssh/authorized_keys

chmod 700 /home/$username/.ssh
chmod 600 /home/$username/.ssh/authorized_keys

# Set proper permissions for user
chown -R $username:users /home/$username/

#Remove and recreate a more hardened iptables default ruleset
rm /etc/iptables/iptables.rules
rm /etc/iptables/ip6tables.rules

cp $MODULEDIR/configs/ipv4.rules /etc/iptables/iptables.rules
cp $MODULEDIR/configs/ipv6.rules /etc/iptables/ip6tables.rules

sed -i "s/dport 12567/dport ${ssh_port}/g" /etc/iptables/iptables.rules
sed -i "s/dport 12567/dport ${ssh_port}/g" /etc/iptables/ip6tables.rules

iptables-restore < /etc/iptables/iptables.rules
ip6tables-restore < /etc/iptables/ip6tables.rules

# Set netctl profile
cp $MODULEDIR/configs/static_linode /etc/netctl/static_linode

sed -i "s/IPv4_GW/${ipv4_gw}/g" /etc/netctl/static_linode
sed -i "s/IPv4/${ssh_ipv4}/g" /etc/netctl/static_linode
sed -i "s/IPv6_GW/${ipv6_gw}/g" /etc/netctl/static_linode
sed -i "s/IPv6/${ssh_ipv6}/g" /etc/netctl/static_linode
