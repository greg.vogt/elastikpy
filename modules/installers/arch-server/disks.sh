#!/bin/bash

# Force unmount any disks so we can nuke them
umount -f /dev/${disk}* 

# Make sure disk is empty
sfdisk --delete /dev/$disk 

# Partition scheme
expect -c "
set timeout -1
spawn parted /dev/$disk mklabel gpt

expect \"Yes/No?\"
send -- \"Yes\r\"

expect eof
"

echo "mkpart primary 1MiB 3MiB
set 1 bios_grub on
mkpart ESP fat32 3MiB 300MiB
set 2 boot on
mkpart primary ext4 300MiB 100%
quit
" | parted /dev/$disk

####################################################################################################
# Expect and Cryptsetup do not play nice with eachother,                                           #
# if anyone knows why please feel free to do a pull request                                        #
# or email me: greg.vogt@vogson.io.                                                                #
#                                                                                                  #
# Fix for automating this:                                                                         #
# https://www.reddit.com/r/linuxadmin/comments/3nl7vc/need_help_automating_cryptsetup_bash_script/ #
####################################################################################################
# Create cryptographic mapper device in LUKS
echo ${disk_password} | cryptsetup -q --verbose --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random luksFormat /dev/${disk}3

# expect -dc "
# set timeout -1

# spawn cryptsetup --verbose --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random luksFormat /dev/${disk}3

# expect \"Are you sure? (Type uppercase yes):\"
# send -- \"YES\r\"

# expect \"Enter passphrase for /dev/${disk}3:\"
# send -- \"${disk_password}\r\"

# expect \"Verify passphrase:\"
# send -- \"${disk_password}\r\"

# expect eof
# "

# Open encrypted device
echo ${disk_password} | cryptsetup open --type luks /dev/${disk}3 cryptroot

# expect -c "
# set timeout -1

# spawn cryptsetup open --type luks /dev/${disk}3 cryptroot

# expect \"Enter passphrase for /dev/${disk}3:\"
# send -- \"${disk_password}\r\"

# expect eof
# "

# Format partitions
mkfs.fat -F32 /dev/${disk}2
mkfs.ext4 /dev/mapper/cryptroot

# Mount partitions
mount /dev/mapper/cryptroot /mnt
mkdir /mnt/boot
mount /dev/${disk}2 /mnt/boot

# VERIFIED WORKING