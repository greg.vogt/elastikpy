#!/bin/bash

######################
# JSON TO BASH ARRAY #
######################

# A useful hack to convert GPG encrypted json files to bash variables

# Usage json "0/1" "file-in" "file-out"
# 0 : Not encrypted
# 1 : Encrypted
# "file-out" is optional if gpg is not used
json(){
    if [ -f $2 ]; then

        # So jq knows where to find the file
        file=$2

        if [ $1 -eq "1" ]; then
            # Decrypt our file 
            gpg -d $2 > $3
            file=$3
        fi

        # Convert json to bash variables
        declare -A conf
        while IFS="=" read -r key value; do
            conf[$key]="$value"
        done < <(jq -r 'to_entries|map("\(.key)=\(.value|tostring)")|.[]' $file)

        for key in "${!conf[@]}"; do
            export $key="${conf[$key]}"
        done
    else
        echo "$1 doesn't exist!";
        exit 1;
    fi
}
