### ElastikPy: a flexible utility for hardening systems

Hardening a new system can take hours of repetitiveness, making it easy to get lost in the monotony of the task and forget cruical steps that could negatively affect you done the road.

ElastikPy aims to condense those tasks down to minutes by leveraging a modular design. It will handle basic setup of the environemnt to create a stable foundation to build from. The pre-packaged Arch-Server module is a fine example of how to leverage this tool. Using the index.json file it can run scripts in a specific order if you simply need to bootstrap the environemnt to a hard set of rules. Or, should you need more control of the process, and index.sh file can be used to tell ElastikPy to hand off full control to your script. Once your script completes it will automatically hand back off to ElastikPy to run clean up. 
