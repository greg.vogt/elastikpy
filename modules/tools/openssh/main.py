#!/usr/bin/env python

# Allows us to import modules and scripts from our vendor subfolder.
import os, sys, signal
from os.path import dirname, join, abspath

# Get working directory
dirpath = abspath(join(dirname(__file__)))

# Add vendor to system path
sys.path.append(sys.argv[2] + "/vendor")

# Vendor imports
from dialog import Dialog
from OutStream import OutStream
from vogson.random import ran_gen
from vogson.dwrapper import mixedform
from vogson.misc import merge

import locale, json, string, random, subprocess

script = "OpenSSH Tools"

# This is almost always a good thing to do at the beginning of your programs.
locale.setlocale(locale.LC_ALL, '')

# You may want to use 'autowidgetsize=True' here (requires pythondialog >= 3.1)
d = Dialog(dialog="dialog", autowidgetsize=True)

# Dialog.set_background_title() requires pythondialog 2.13 or later
d.set_background_title(sys.argv[1] + " - " + script)

button_names = {d.OK:     "OK",
                d.CANCEL: "Cancel",
                d.HELP:   "Help",
                d.EXTRA:  "Extra"}

# Object for all values to be JSON enocded
values = {}

interfaces = netifaces.interfaces()


code, string = d.inputbox("Please choose the disk you'd like to install to: \n" + out.decode('utf-8'))

values['disk'] = (string, "disk")



# Usage ("Label", "Default Text", "Environment Variable")
items = [
    ("IPv4", "", "ipv4"),
    ("IPv6", "", "ipv6"),
    ("Username", "", "username"),
    ("Password", "", "password"),
    ("SSH Key", "", "ssh_key")
]

result = mixedform(d, items)
json_out = {}

# Probably a better way to do this, just combines values and res together
for i in range(len(result)):
    json_out[items[i][2]] = result[items[i][0]]

for i in values:
    json_out[values[i][1]] = values[i][0]

filename = ran_gen(10) + ".json"

f = open(sys.argv[2] + "/volatile/" + filename, "w")
f.write(json.dumps(json_out))
f.close()

print(json_out)

d.msgbox("Please note that these variables are stored in " + dirpath + "/volatile/" + filename + " in PLAIN TEXT. It is recommended to use Ctrl+C or the scripts Exit buttons so that this folder is cleared properly.")

#OutStream.run([dirpath + "/index.sh", sys.argv[2], filename], script, "flatfile", True)

