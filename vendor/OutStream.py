#!/usr/bin/env python3
#
# Written 2017, 2019 by Tobias Brink
#
# To the extent possible under law, the author(s) have dedicated
# all copyright and related and neighboring rights to this software
# to the public domain worldwide. This software is distributed
# without any warranty.
#
# You should have received a copy of the CC0 Public Domain
# Dedication along with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.
#
#
# https://tbrink.science/blog/2017/04/30/processing-the-output-of-a-subprocess-with-python-in-realtime/

# For OutSream class
import errno, os, signal

# For run function
import pty, select, subprocess
from os import path
from datetime import datetime

# Set signal handler for SIGINT.
signal.signal(signal.SIGINT, lambda s,f: print("received SIGINT"))


class OutStream:
    def __init__(self, fileno):
        self._fileno = fileno
        self._buffer = b""

    def read_lines(self):
        try:
            output = os.read(self._fileno, 1000)
        except OSError as e:
            if e.errno != errno.EIO: raise
            output = b""
        lines = output.split(b"\n")
        lines[0] = self._buffer + lines[0] # prepend previous
                                           # non-finished line.
        if output:
            self._buffer = lines[-1]
            finished_lines = lines[:-1]
            readable = True
        else:
            self._buffer = b""
            if len(lines) == 1 and not lines[0]:
                # We did not have buffer left, so no output at all.
                lines = []
            finished_lines = lines
            readable = False
        finished_lines = [line.rstrip(b"\r").decode()
                          for line in finished_lines]
        return finished_lines, readable

    def fileno(self):
        return self._fileno

    # Example for future reference
    def run(process, module, log_type="flatfile", clear=False):
        # If clear is true clear console before launching process 
        if clear is True:
            os.system('cls' if os.name == 'nt' else 'clear')

        # Start the subprocess.
        out_r, out_w = pty.openpty()
        err_r, err_w = pty.openpty()
        proc = subprocess.Popen(process, stdout=out_w, stderr=err_w)
        os.close(out_w) # if we do not write to process, close these.
        os.close(err_w)

        fds = {OutStream(out_r), OutStream(err_r)}

        if log_type == "flatfile":
            file = open(process[1] + "/logs/" + module + "_" + datetime.now().strftime("%d-%m-%Y_%H-%M-%S") + ".log","w+")
        while fds:
            # Call select(), anticipating interruption by signals.
            while True:
                try:
                    rlist, _, _ = select.select(fds, [], [])
                    break
                except InterruptedError:
                    continue
            # Handle all file descriptors that are ready.
            for f in rlist:
                lines, readable = f.read_lines()
                # Example: Just print every line. Add your real code here.
                for line in lines:
                    if log_type == "flatfile":
                        # Prints lines to console after adding running process and datetime stamp
                        cur_line = "[" + module + "]" + datetime.now().strftime("[%d-%m-%Y %H:%M:%S]") + ": " + line
                        print(cur_line)
                        file.write(cur_line + "\r\n") # Add new line for log
                if not readable:
                    if log_type == "flatfile":
                        file.close()
                    # This OutStream is finished.
                    fds.remove(f)