#!/usr/bin/env python
import sys, signal, os, shutil, inspect

# How to use function
# import signal
# from vogson.signal import signal_handler

# Usage: signal.signal(signal.SIGINT, signal_handler)

# Configured via elastik.sh: sed -i "s#/root/ElastikPy/volatile#${PWD}/volatile#g" $PWD/vendor/vogson/signal.py
volatile_path = "/elastikpy/volatile"
message = "SIGINT caught, cleaning up and exiting"

# https://stackoverflow.com/a/1112350
def signal_handler(sig = None, frame = None):
    global message
    global volatile_path
    
    print(message)

    # https://stackoverflow.com/a/185941
    for filename in os.listdir(volatile_path):
        file_path = os.path.join(volatile_path, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                print("Removing: " + file_path)
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                print("Removing: " + file_path)
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

    sys.exit(0)
