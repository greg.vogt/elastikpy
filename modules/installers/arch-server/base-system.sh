# No bash starter needed since this will be included in chroot-runner.sh

pacman -S --noconfirm figlet parted expect

figlet "Installation"

# set hostname
echo $hostname > /etc/hostname

# Use expect to set root password
expect -c "
set timeout -1
spawn passwd

expect \"New password:\"
send -- \"$root_password\r\"
expect \"Retype new password:\"
send -- \"$root_password\r\"

expect eof
"

# Set timezone (or use tzselect)
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
hwclock --systohc

# Set locales and generate them
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "LANGUAGE=en_US" >> /etc/locale.conf
echo "KEYMAP=us" > /etc/vconsole.conf
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
echo "en_US ISO-8859-1" >> /etc/locale.gen
locale-gen

# Fix pacman: Signature is unknown trust
figlet Generating Pacman init
rm -Rf /etc/pacman.d/gnupg
pacman-key --init
pacman-key --populate archlinux

# Update system and install Intel or AMD microcode
pacman -Syyu --noconfirm
#pacman -S --noconfirm amd-ucode
pacman -S --noconfirm intel-ucode

# Install LTS Kernel and headers
figlet LTS Kernel and Headers
pacman -S --noconfirm linux-lts linux-lts-headers

# Configure mkinitcpio
# 3/2/20 Note: Occasionally this file can be reset by an upgrade and
# will render the next boot unable to find the disk. 
sed -i '/^HOOKS=/s/block/block encrypt/g' /etc/mkinitcpio.conf

# Rebuild inital ram disk
figlet Rebuilding RAM Disk
mkinitcpio -p linux

# Regarding the above note about mkinitcpio.conf getting reset due to
# updates/upgrades you'll need to make sure to run this preset as well
# I just saved you ~3 hours of research...
mkinitcpio -p linux-lts

# Set up GRUB
figlet GRUB Installation

# Set bootloader
if [[ $bootloader = "UEFI" ]] ; then
    grub="--target=x86_64-efi --efi-directory=/boot"
else
    grub="--target=i386-pc /dev/${disk}"
fi

pacman -S --noconfirm grub os-prober efibootmgr dosfstools mtools
os-prober
# 3/2/20 Note: Occasionally this file can be reset by an upgrade and
# will render the next boot unable to find the disk. 
sed -i "s#GRUB_CMDLINE_LINUX=\"\"#GRUB_CMDLINE_LINUX=\"cryptdevice=/dev/${disk}3:cryptroot\"#g" /etc/default/grub
grub-install $grub --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg