#!/bin/bash

{
    echo "Raw output:"
    echo ""
    echo ""
    
    arch-audit
    echo ""
    echo ""
    
    echo "Below are links to the CVE's listed above:"
    echo ""
    echo ""

    for i in $(arch-audit -f %n:%v:%c); do
        while IFS=':' read -ra ADDR; do

            result="${ADDR[0]}: ${ADDR[2]} - Fixed: ${ADDR[1]}"

            if [[ -z ${ADDR[1]} ]]; then
                result="${ADDR[0]}: ${ADDR[2]} - No fixed version available!"
            fi

            cve=${ADDR[2]}
        done <<< "$i"

        echo "$result"
        echo "https://cve.mitre.org/cgi-bin/cvename.cgi?name=$cve"
        echo "https://nvd.nist.gov/vuln/detail/$cve"

        echo "---------------------------------------------------"
    done

} | mail -s "Arch-Audit check for $HOSTNAME" email