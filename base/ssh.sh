#!/bin/bash

input_box "$MODULE" "Root Password" "passwordbox" "Please enter the root password" "" "true" "Please confirm your password:"
root_password="$tmp_output"

expect -c "
set timeout -1
spawn passwd

expect \"New password:\"
send -- \"$root_password\r\"
expect \"Retype new password:\"
send -- \"$root_password\r\"

expect eof
"

cp $PWD/modules/arch-server.mod/configs/sshd.conf /etc/ssh/sshd_config

#Ask user for SSH port
input_box "SSH" "Port" "inputbox" "SSH Port (Current: 21001): " "21001" "false" ""
ssh_port="$tmp_output"

if [ -z "$ssh_port" ]; then
    sed -i "s/Port 21001/Port $ssh_port/g" /etc/pam.d/system-auth
fi

#Remove and recreate a more hardened iptables default ruleset
rm /etc/iptables/iptables.rules
rm /etc/iptables/ip6tables.rules

cp $PWD/modules/arch-server.mod/configs/ipv4.rules /etc/iptables/iptables.rules
cp $PWD/modules/arch-server.mod/configs/ipv6.rules /etc/iptables/ip6tables.rules

sed -i "s/dport 12567/dport ${ssh_port}/g" /etc/iptables/iptables.rules
sed -i "s/dport 12567/dport ${ssh_port}/g" /etc/iptables/ip6tables.rules

iptables-restore < /etc/iptables/iptables.rules
ip6tables-restore < /etc/iptables/ip6tables.rules

# Remove publickey requirement since we are using password authentication
sed -i "s/publickey,//g" /etc/ssh/sshd_config

# Allow root login for our install
sed -i "s/PermitRootLogin no/PermitRootLogin yes/g" /etc/ssh/sshd_config
sed -i "s/AllowUsers/AllowUsers root/g" /etc/ssh/sshd_config
sed -i "s/AllowGroups ssh-users/AllowGroups root ssh-users/g" /etc/ssh/sshd_config
sed -i "s/DenyUsers root/DenyUsers/g" /etc/ssh/sshd_config
sed -i "s/DenyGroups root/DenyGroups/g" /etc/ssh/sshd_config

systemctl start sshd