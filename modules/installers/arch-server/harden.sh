# No bash starter needed since this will be included in chroot-runner.sh

set -e
echo "Changing umask permissions..."
umask 0027

echo "Setting Environmental variables for Language options..."
export LANG=C
export LANGUAGE=en_US

# Needed for hardening SSH with allowgroups
groupadd ssh-users

# Add our user to the group
usermod -a -G ssh-users $username

cat $MODULEDIR/configs/makepkg.conf > /etc/makepkg.conf

echo "Applying Hardened configuration files"
cat $MODULEDIR/configs/aide.conf > /etc/aide.conf
cat $MODULEDIR/configs/bash.bashrc > /etc/bash.bashrc
cat $MODULEDIR/configs/default-passwd.conf > /etc/default/passwd
# cat $MODULEDIR/configs/default-useradd.conf > /etc/default/useradd
cat $MODULEDIR/configs/hardening-wrapper.conf > /etc/hardening-wrapper.conf

cat $MODULEDIR/configs/issue > /etc/issue
cat $MODULEDIR/configs/issue-net > /etc/issue.net

# cat $MODULEDIR/configs/locale.conf > /etc/locale.conf
# cat $MODULEDIR/configs/locale.gen > /etc/locale.gen

# locale-gen

# cat $MODULEDIR/configs/login.defs > /etc/login.defs
# cat $MODULEDIR/configs/logrotate.conf > /etc/logrotate.conf # Not currently installed
cat $MODULEDIR/configs/modprobe-d-blacklist-firewire > /etc/modprobe.d/blacklist-firewire
cat $MODULEDIR/configs/modprobe-d-blacklist-usb > /etc/modprobe.d/blacklist-usb
# cat $MODULEDIR/configs/profile > /etc/profile
# cat $MODULEDIR/configs/securetty > /etc/securetty
# cat $MODULEDIR/configs/security-access.conf > /etc/security/access.conf

# sed -i "s/username/${username}/g" /etc/security/access.conf

# cat $MODULEDIR/configs/security-group.conf > /etc/security/group.conf
# cat $MODULEDIR/configs/security-limits.conf > /etc/security/limits.conf
# cat $MODULEDIR/configs/security-namespace.conf > /etc/security/namespace.conf
# cat $MODULEDIR/configs/security-pam-env.conf > /etc/security/pam_env.conf
# cat $MODULEDIR/configs/security-time.conf > /etc/security/time.conf
# cat $MODULEDIR/configs/shells > /etc/shells
cat $MODULEDIR/configs/ssh-ssh_config > /etc/ssh/ssh_config
cat $MODULEDIR/configs/ssh-sshd_config > /etc/ssh/sshd_config
# cat $MODULEDIR/configs/sudoers > /etc/sudoers
cat $MODULEDIR/configs/sysctl-d-05-grsecurity.conf > /etc/sysctl.d/05-grsecurity.conf

# May be smart to join this with the network harden below
cat $MODULEDIR/configs/sysctl-d-10-sysctl.conf > /etc/sysctl.d/10-sysctl.conf

# Setting audtid rules
cat $MODULEDIR/configs/auditd.conf > /etc/audit/auditd.conf

# Configure SSH
sed -i "s/21001/$ssh_port/g" /etc/ssh/sshd_config
sed -i "s/0.0.0.0/$ssh_ipv4/g" /etc/ssh/sshd_config
sed -i "s/::/$ssh_ipv6/g" /etc/ssh/sshd_config

# Remove unused host keys and generate new ones
rm /etc/ssh/ssh_host_*key*
ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N "" < /dev/null
ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N "" < /dev/null

# Configure auditd to pass events to syslog
sed -i 's/active = no/active = yes/g' /etc/audisp/plugins.d/syslog.conf

# Enable Kernel Auditing
sed -i 's/GRUB_CMDLINE_LINUX="/GRUB_CMDLINE_LINUX="audit=1 /g' /etc/default/grub

grub-mkconfig -o /boot/grub/grub.cfg

# Disable core dumps
echo "*  hard  core  0" >> /etc/security/limits.conf

# Hardening TCP Wrappers
cat <<EOT >> /etc/hosts.allow
ALL: 127.0.0.1
sshd: ALL
EOT
echo "ALL: ALL" > /etc/hosts.deny

# Kernel Network Hardening
cat <<EOT >> /etc/sysctl.d/10-harden.conf
# Disable packet forwarding
net.ipv4.ip_forward = 0

# Disable redirects, not a router
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.default.secure_redirects = 0
net.ipv6.conf.all.accept_redirects = 0
net.ipv6.conf.default.accept_redirects = 0

# Disable source routing
net.ipv4.conf.all.accept_source_route = 0
net.ipv4.conf.default.accept_source_route = 0
net.ipv6.conf.all.accept_source_route = 0

# Enable source validation by reversed path
net.ipv4.conf.all.rp_filter = 1
net.ipv4.conf.default.rp_filter = 1

# Log packets with impossible addresses to kernel log
net.ipv4.conf.all.log_martians = 1
net.ipv4.conf.default.log_martians = 1

# Disable ICMP broadcasts
net.ipv4.icmp_echo_ignore_broadcasts = 1

# Ignore bogus ICMP errors
net.ipv4.icmp_ignore_bogus_error_responses = 1

# Against SYN flood attacks
net.ipv4.tcp_syncookies = 1

# Turning off timestamps could improve security but degrade performance.
# TCP timestamps are used to improve performance as well as protect against
# late packets messing up your data flow. A side effect of this feature is
# that the uptime of the host can sometimes be computed.
# If you disable TCP timestamps, you should expect worse performance
# and less reliable connections.
net.ipv4.tcp_timestamps = 1
EOT

# Kernel Network Module hardening
cat <<EOT >> /etc/modprobe.d/hardening.conf
install bnep /bin/true
install bluetooth /bin/true
install btusb /bin/true
install net-pf-31 /bin/true
install appletalk /bin/true
install dccp /bin/true
install sctp /bin/true
install rds /bin/true
install tipc /bin/true
EOT

# Disable LLMNR
sed -i "s/#LLMNR=yes/LLMNR=no/g" /etc/systemd/resolved.conf

# Add nginx 404 filter
# Fail2ban config
cat $MODULEDIR/configs/fail2ban-jails/00-defaults.conf > /etc/fail2ban/jail.d/00-defaults.conf
cat $MODULEDIR/configs/fail2ban-jails/10-nginx.conf > /etc/fail2ban/jail.d/10-nginx.conf
cat $MODULEDIR/configs/fail2ban-jails/10-ssh.conf > /etc/fail2ban/jail.d/10-ssh.conf

sed -i "s/EMAIL/$email/g" /etc/fail2ban/jail.d/00-defaults.conf
sed -i "s/HOSTNAME/$username@$hostname/g" /etc/fail2ban/jail.d/00-defaults.conf
sed -i "s/21001/$ssh_port/g" /etc/fail2ban/jail.d//10-ssh.conf

# Configure email
cat $MODULEDIR/configs/msmtprc > /etc/msmtprc

sed -i "s/email_host/$email_host/g" /etc/msmtprc
sed -i "s/username@hostname/$username@$hostname/g" /etc/msmtprc
sed -i "s/email_user/$email_user/g" /etc/msmtprc
sed -i "s/email_password/$email_password/g" /etc/msmtprc

# Create AIDE crontab for periodic execution and email results
cat $MODULEDIR/configs/aide.sh > /etc/cron.hourly/aide.sh
chmod +x /etc/cron.hourly/aide.sh

sed -i "s/email=\"\"/email=\"$email\"/g" /etc/cron.hourly/aide.sh

# Create Pacman crontab for periodic execution and email results
cat $MODULEDIR/configs/pacman-update.sh > /etc/cron.daily/pacman-update.sh
chmod +x /etc/cron.daily/pacman-update.sh

sed -i "s/email/$email/g" /etc/cron.daily/pacman-update.sh

# Create Arch-Audit crontab for periodic execution and email results
cat $MODULEDIR/configs/arch-audit.sh > /etc/cron.daily/arch-audit.sh
chmod +x /etc/cron.daily/arch-audit.sh

sed -i "s/email/$email/g" /etc/cron.daily/arch-audit.sh

# Setup logwatch
cp /usr/share/logwatch/default.conf/logwatch.conf /etc/logwatch/conf/logwatch.conf
cp -R /usr/share/logwatch/scripts/services/ /etc/logwatch/scripts/

sed -i "s/Output = stdout/Output = email/g" /etc/logwatch/conf/logwatch.conf
sed -i "s/MailTo = root/MailTo = $email/g" /etc/logwatch/conf/logwatch.conf
sed -i "s/MailFrom = Logwatch/MailFrom = $user@$HOSTNAME/g" /etc/logwatch/conf/logwatch.conf
sed -i "s/Detail = Low/Detail = High/g" /etc/logwatch/conf/logwatch.conf

# Prevent empty passwords 
sed -i 's/\<nullok\>//g' /etc/pam.d/system-auth
sed -i 's/\<nullok\>//g' /etc/pam.d/passwd