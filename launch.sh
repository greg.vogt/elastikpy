#!/bin/bash

################################################################################
# Help                                                                         #
################################################################################
help()
{
   # Display Help
   echo "Builds or launches ElastikPy Docker container"
   echo
   echo "Syntax: launch.sh [-g|h|v|V] <options>"
   echo "options:"
   echo "r     Run ElastikPy"
   echo "b     Build ElastikPy"
   echo "h     Print this Help."
   echo
}

while getopts ':hbr' option; do
  case "$option" in
    h)  help
        exit
        ;;
    b)  docker build . --tag elastikpy:latest
        ;;
    r)  docker run -v $PWD:/elastikpy -v ${HOME}/.gnupg/:/root/.gnupg/:ro -t -i --privileged elastikpy
        ;;
    :)  printf "missing argument for -%s\n" "$OPTARG" >&2
        help >&2
        exit 1
        ;;
    \?)  printf "illegal option: -%s\n" "$OPTARG" >&2
        help >&2
        exit 1
        ;;
  esac
done