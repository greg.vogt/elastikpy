#!/usr/bin/env python

# Allows us to import modules and scripts from our vendor subfolder.
import os, sys, signal
from os.path import dirname, join, abspath

dirpath = abspath(join(dirname(__file__)))

sys.path.append(dirpath + "/vendor")

# Vendor imports
from dialog import Dialog
from vogson.signal import signal_handler
from vogson.dwrapper import menu

signal.signal(signal.SIGINT, signal_handler)

import locale, json, subprocess

# This is almost always a good thing to do at the beginning of your programs.
locale.setlocale(locale.LC_ALL, '')

# You may want to use 'autowidgetsize=True' here (requires pythondialog >= 3.1)
d = Dialog(dialog="dialog", autowidgetsize=True)

# Dialog.set_background_title() requires pythondialog 2.13 or later
d.set_background_title(sys.argv[1])

button_names = {d.OK:     "OK",
                d.CANCEL: "Cancel",
                d.HELP:   "Help",
                d.EXTRA:  "Extra"}

# Gets the categories and displays them; returns path of selected category
main_menu = menu(sys.argv[1], d, dirpath + "/modules", dirpath, type = "category")

# Triggers dir scan for modules; returns nothing currently
module_menu = menu(sys.argv[1], d, main_menu, dirpath)
